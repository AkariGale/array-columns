import ast
import pandas as pd
from numpy.core import find_common_type
from pandas._libs import lib
from pandas.core.dtypes.cast import astype_nansafe
from pandas.api.extensions import ExtensionArray, ExtensionDtype, register_extension_dtype
from pandas.api.types import is_integer_dtype, is_bool_dtype, is_object_dtype, is_float_dtype, is_string_dtype
from pandas.api.types import pandas_dtype, is_integer, is_list_like
from pandas.core.common import is_bool_indexer


class OneDimArray(ExtensionArray):
    '''These are a few classes for working with arrays that Pandas respectfully stores in dataframes and series,
        and which can be customized for our use (for example, writing to a csv in a format compatible with postgres)'''
    _data = None
    _mask = None
    __array_priority__ = 1000

    def __init__(self, values, dtype, mask=None, copy=False):

        if dtype is None and hasattr(values, "dtype"):
            if isinstance(values.dtype, AbstractDtype):
                dtype = values.dtype
        if dtype is not None:
            if isinstance(dtype, str):
                try:
                    dtype = AbstractDtype.construct_from_string(dtype)
                except TypeError:
                    raise ValueError(f"invalid dtype specified {dtype}")

        if isinstance(values, OneDimArray):
            values, mask = values._data, values._mask
            if dtype is not None:
                values = values.astype(dtype.kind, copy=False)
            if copy:
                values = values.copy()
                mask = mask.copy()

        self._dtype = dtype
        if dtype.character_code == 'U' and pd.isna(dtype.itemsize):
            dtype.itemsize = max([len(u) for v in values for u in v])

        assert (pd.np.array([len(v) for v in values]) == dtype.length).all(), \
                'there are arrays whose length is different from the declared'

        if len(values) == 0:
            values = pd.np.array([], dtype=dtype.kind)
        else:
            values = pd.np.array([pd.np.array(v, copy=copy) for v in values])
            if is_object_dtype(values):
                inferred_type = lib.infer_dtype(values, skipna=True)
                if inferred_type == "empty":
                    values = pd.np.empty(len(values))
                    values.fill(None)
                elif inferred_type not in ["floating", "integer", "boolean", "string"]:
                    raise TypeError(f"{values.dtype} cannot be converted to an AbstractDtype")
            elif is_bool_dtype(values):
                if is_integer_dtype(dtype.kind):
                    values = pd.np.array(values, dtype=dtype.kind, copy=copy)
                else:
                    assert is_bool_dtype(dtype.kind), f"{values.dtype} cannot be converted to an BoolDtype"
            elif is_integer_dtype(values):
                if is_float_dtype(dtype.kind):
                    values = pd.np.array(values, dtype=dtype.kind, copy=copy)
                else:
                    assert is_integer_dtype(dtype.kind), f"{values.dtype} cannot be converted to an IntegerDtype"
            elif is_float_dtype(values):
                assert is_float_dtype(dtype.kind), f"{values.dtype} cannot be converted to an FloatDtype"
            elif is_string_dtype(values):
                assert is_string_dtype(dtype.kind), f"{values.dtype} cannot be converted to an UnicodeDtype"
            else:
                raise TypeError(f"{values.dtype} cannot be converted to an AbstractDtype")

        if mask is None:
            if values.ndim == 2:
                mask = pd.np.array([False] * len(values))
            else:
                vect_is_list_like = pd.np.vectorize(is_list_like)
                mask = vect_is_list_like(values)
        else:
            assert len(mask) == len(values), 'length of the mask and array must be equal'

        if mask.any():
            values = values.copy()
            values[mask] = self.dtype.unit_value

        if len(values) > 0:
            if not values.ndim == 2: raise TypeError("values must be a 2D list-like")
        if not mask.ndim == 1: raise TypeError("mask must be a 1D list-like")

        self._data = values
        self._mask = mask

    @classmethod
    def _from_sequence(cls, scalars, dtype, copy=False):
        return cls(scalars, dtype, copy=copy)

    @classmethod
    def _from_sequence_of_strings(cls, strings, dtype, copy=False):
        return cls([ast.literal_eval(i) for i in strings], dtype, copy=copy)

    @classmethod
    def _from_factorized(cls, values, original):
        return cls(values, original.dtype)

    def __getitem__(self, item):
        if is_integer(item):
            if self._mask[item]:
                return self.dtype.na_value
            return self._data[item]
        return type(self)(self._data[item], self.dtype, self._mask[item])

    def _coerce_to_ndarray(self):
        data = self._data.astype(object)
        data[self._mask] = self.dtype.na_value
        return pd.np.array([()] + [x for x in data])[1:]

    def __array__(self, dtype=None):
        return self._coerce_to_ndarray()

    def __len__(self):
        return len(self._data)

    @property
    def dtype(self):
        return self._dtype

    @property
    def shape(self):
        return (len(self), self.dtype.itemsize)

    @property
    def nbytes(self):
        return self._data.nbytes + self._mask.nbytes

    @property
    def kind(self):
        return self.dtype.character_code

    @property
    def _ndarray_values(self):
        return self._data

    def isna(self):
        return self._mask

    def _values_for_argsort(self):
        return self._data

    def unique(self):
        return type(self)(pd.np.unique(self._data), self.dtype)

    def _values_for_factorize(self):
        return self._values_for_argsort(), self.dtype.na_value

    def _values_for_argsort(self):
        return pd.np.array([()] + [tuple(a) for a in self._data], dtype='object')[1:]

    def __iter__(self):
        for i in range(len(self)):
            if self._mask[i]:
                yield self.dtype.na_value
            else:
                yield self._data[i]

    def take(self, indices, allow_fill=False, fill_value=None):
        from pandas.core.algorithms import take
        print('kek')
        fill_value = self.dtype.unit_value if pd.isna(fill_value) else fill_value
        result = take(self._data, indices, fill_value=fill_value, allow_fill=allow_fill)
        mask = take(self._mask, indices, fill_value=True, allow_fill=allow_fill)
        if allow_fill and pd.notna(fill_value):
            fill_mask = pd.np.asarray(indices) == -1
            result[fill_mask] = fill_value
            mask = mask ^ fill_mask
        return type(self)(result, self.dtype, mask)

    def copy(self):
        return type(self)(self._data, self.dtype, self._mask, True)

    def __setitem__(self, key, value):
        print('kek')

        if value.ndim == 1:
            value = [value]
        value, mask = OneDimArray(value, dtype=self.dtype)
        if value.ndim == 1:
            value = value[0]
            mask = mask[0]
        self._data[key] = value
        self._mask[key] = mask

    @classmethod
    def _concat_same_type(cls, to_concat):
        data = pd.np.concatenate([x._data for x in to_concat])
        mask = pd.np.concatenate([x._mask for x in to_concat])
        return cls(data, to_concat[0].dtype, mask)

    @classmethod
    def get_array_dtype(cls, dtype):
        dtype_dict = {'?': BoolArray, 'i': IntegerArray, 'f': FloatArray, 'U': UnicodeDtype,
                      'M': TimestampArray, 'm': TimedeltaArray}
        return dtype_dict.get(dtype.kind, None)

    def __repr__(self):
        rows = self._formatting_values()
        return ", ".join(rows)

    def _formatting_values(self):
        return pd.np.apply_along_axis(lambda x: "{"  + ', '.join([str(y) for y in x]) + "}", 1, self._data)

    def isin(self, other):
        it = iter(other)
        if type(self).__name__ == type(other).__name__ and all(d in it for d in self):
           return True
        return False

    def __eq__(self, other):
        if  type(self).__name__ == type(other).__name__ and len(self) == len(other) \
             and (self._data == other._data).all():
            return True
        return False

    def __hash__(self):
        return hash(str(pd.util.hash_array(self._data)))

    def astype(self, dtype, copy=True):
        if isinstance(dtype, type(self.dtype)):
            return type(self)(self._data, self.dtype, mask=self._mask, copy=False)
        data = self._coerce_to_ndarray()
        data = astype_nansafe(data, dtype, copy=None)
        return data


class AbstractDtype(ExtensionDtype):

    itemsize = pd.np.nan
    length = pd.np.nan
    _metadata = ("character_code", "itemsize", "length")
    character_code = None
    unit_scalar = None

    def __init__(self, length, itemsize):
        assert type(self) != AbstractDtype, 'Parent class not intended for direct use'
        assert not pd.isna(length), 'you must specify a fixed length of arrays'
        self.length = length
        self.itemsize = itemsize

    @property
    def na_value(self):
        return pd.np.array([None] * self.length, dtype='object')

    @property
    def unit_value(self):
        return pd.np.array([self.unit_scalar] * self.length, dtype=self.kind)

    @property
    def type(self):
        return pd.np.ndarray

    @property
    def kind(self):
        return self.character_code + (str(self.itemsize) if self.character_code != '?' else '')

    @property
    def name(self):
        return f'{find_common_type([self.kind], [])}[{self.length if is_integer(self.length) else ""}]'

    @classmethod
    def construct_from_string(cls, string):
        try:
            length_begin = string.rfind('[') + 1
            alphas_end = max([i for i in range(len(string)) if string[i].isalpha()]) + 1
            data_type = string[0: alphas_end][0]
            length = int(string[length_begin: -1])
        except ValueError:
            raise TypeError(f"Cannot construct a '{cls.__name__}' from '{string}'")
        itemsize = int(string[alphas_end: length_begin - 1]) if length_begin - alphas_end > 1 else None
        if itemsize is not None: itemsize //= 8
        if len(data_type) > 0 and length is not None:
            dclass = cls.get_dtype(data_type[0])
            return dclass(length, itemsize)

    @classmethod
    def get_dtype(cls, kind):
        dtype_dict = {'?': BoolDtype, 'i': IntegerDtype, 'f': FloatDtype, 'U': UnicodeDtype,
                      'M': TimestampDtype, 'm': TimedeltaDtype}
        return dtype_dict.get(kind, None)


@register_extension_dtype
class BoolDtype(AbstractDtype):

    character_code = '?'
    unit_scalar = True
    _is_numeric = True
    _is_boolean = True

    def __init__(self, length=pd.np.nan):
        super(BoolDtype, self).__init__(length, 1)

    @classmethod
    def construct_array_type(cls):
        return BoolArray


class BoolArray(OneDimArray):

    def __init__(self, values, dtype, mask=None, copy=False):
        super(BoolArray, self).__init__(values, dtype, mask, copy)


@register_extension_dtype
class UnicodeDtype(AbstractDtype):

    character_code = 'U'
    unit_scalar = ''

    def __init__(self, length=pd.np.nan, itemsize=pd.np.nan):
        super(UnicodeDtype, self).__init__(length, itemsize)

    @property
    def name(self):
        return f'Unicode[{self.length if is_integer(self.length) and self.length < pd.np.inf else ""}]'

    @classmethod
    def construct_array_type(cls):
        return UnicodeArray


class UnicodeArray(OneDimArray):

    def __init__(self, values, dtype, mask=None, copy=False):
        super(UnicodeArray, self).__init__(values, dtype, mask, copy)


@register_extension_dtype
class FloatDtype(AbstractDtype):

    character_code = 'f'
    unit_scalar = 1.0
    _is_numeric = True

    def __init__(self, length=pd.np.nan, itemsize=8):
        super(FloatDtype, self).__init__(length, itemsize)

    @classmethod
    def construct_array_type(cls):
        return FloatArray


class FloatArray(OneDimArray):

    def __init__(self, values, dtype, mask=None, copy=False):
        super(FloatArray, self).__init__(values, dtype, mask, copy)


@register_extension_dtype
class IntegerDtype(AbstractDtype):

    character_code = 'i'
    unit_scalar = 1
    _is_numeric = True

    def __init__(self, length=pd.np.nan, itemsize=8):
        super(IntegerDtype, self).__init__(length, itemsize)

    @classmethod
    def construct_array_type(cls):
        return IntegerArray


class IntegerArray(OneDimArray):
    '''Working with specific data types simplifies interaction with arrays and reduces analysis and verification'''

    def __init__(self, values, dtype, mask=None, copy=False):
        super(IntegerArray, self).__init__(values, dtype, mask, copy)


@register_extension_dtype
class TimestampDtype(AbstractDtype):

    character_code = 'M'


class TimestampArray(OneDimArray):

    def __init__(self, values, dtype, mask=None, copy=False):
        super(TimestampArray, self).__init__(values, mask, dtype)


@register_extension_dtype
class TimedeltaDtype(AbstractDtype):

    character_code = 'm'


class TimedeltaArray(OneDimArray):

    def __init__(self, values, dtype, mask=None, copy=False):
        super(TimedeltaArray, self).__init__(values, mask, dtype)


class ArrayTools:

    @staticmethod
    def concat_cols(df, cols, new_col_name):
        '''Combining multiple columns of homogeneous types (arrays and scalars) into an array column'''

        arr_dtypes = pd.Series(({c: dt for (c, dt) in df.dtypes.to_dict().items() if isinstance(dt, AbstractDtype)}))
        cols = pd.np.array(cols)

        dtypes = []
        yet_arrays = pd.np.intersect1d(arr_dtypes.index, cols)
        dtypes += [arr_dtypes[c].kind for c in yet_arrays]
        just_scalars = pd.np.setdiff1d(cols, yet_arrays)
        dtypes += [pandas_dtype(type(c)) for c in just_scalars]
        itemsizes = pd.np.array([df[c].dtype.itemsize for c in cols])
        character_codes = pd.np.array([dt[0] for dt in dtypes])

        max_itemsize = max(itemsizes)
        summary_length = len(just_scalars) + sum([arr_dtypes[c].length for c in yet_arrays])
        assert (character_codes == character_codes[0]).all(), 'columns are not of homogeneous types'
        character_code = character_codes[0]
        print(max_itemsize, summary_length)

        df = df.astype({c: AbstractDtype.get_dtype(character_code)[1, df[c].dtype.itemsize] for c in just_scalars})

        print(df.loc[:, cols])
        def lambda_print(x):
            print(x)
            return pd.np.concatenate(x)
        df[new_col_name] = df.loc[:, cols].apply(lambda_print)
        df.drop(cols, axis=1, inplace=True)
        return df
